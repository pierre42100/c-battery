#include <stdio.h>
#include <stdlib.h>

#define FILE_PATH "/sys/class/power_supply/BAT0/capacity"

int main(int argc, char** argv){

	char * target_path = FILE_PATH;

	if(argc > 1)
		target_path = argv[1];
	

	//Open target file
	FILE *file = fopen(target_path, "r");
	if(file == NULL){
		fprintf(stderr, "Could not open %s for reading!", target_path);
		return EXIT_FAILURE;
	}

	//Get value
	int percent;
	fscanf(file, "%d", &percent);

	//Display value
	if(percent < 20)
		printf("\x1B[31m");
	else if(percent < 35) 
		printf("\x1B[33m");
	else
		printf("\x1B[32m");


	printf("There is %d%% of battery available. \e[0m\n", percent);

	//Exit properly
	fclose(file);
	return EXIT_SUCCESS;
}