# Debian package builder
VERSION=$1
PKG_DIR=battery_$VERSION
echo Build Debian package for version $VERSION in $PKG_DIR

rm -rf $PKG_DIR
mkdir $PKG_DIR

# Create required directories
mkdir -p $PKG_DIR/usr/bin

# Install files
cp ./battery $PKG_DIR/usr/bin


# Create metadata file
mkdir $PKG_DIR/DEBIAN

cat <<EOT > $PKG_DIR/DEBIAN/control
Package: battery
Version: $VERSION
Section: base
Priority: optional
Architecture: amd64
Depends: libc6 (>= 2.14)
Maintainer: Pierre Hubert <pierre.git@communiquons.org>
Description: Battery
 Small software that indicates the level of your battery.
EOT

dpkg-deb --build $PKG_DIR