VERSION="1.0-1"

all: clean battery

battery:
	gcc main.c -o battery

clean:
	rm -f battery
	rm -rf battery_*

run: clean battery
	./battery

install: clean battery
	cp ./battery /usr/bin/

remove: battery
	rm -f /usr/bin/battery


deb: battery
	bash ./make_deb.sh ${VERSION}

.PHONY: all clean
