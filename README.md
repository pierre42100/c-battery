# Battery

In really really tiny command line battery indicator built for Linux systems.

## Author 
Pierre HUBERT

## Licence
The MIT Licence

## Building
```sh
make
```

## Running
```sh
./battery
```

- or -

```sh
make run
```


## Installation
```sh
sudo make install
```

## Uninstall
```sh
sudo make remove
```